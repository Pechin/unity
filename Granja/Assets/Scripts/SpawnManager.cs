﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] animalPrefabs;
    private float spawnRangeX = 20;
    private float spawnPosZ = 30;
    private float lowBound = -14;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //Desaparecer animales
        if (transform.position.z < lowBound)
        {
            Destroy(GameObject);
        };

        if (Input.GetKeyDown(KeyCode.E)) 
        {
            //Posición aleatoria en z del animal que vamos a spawnear
            float xPos= Random.Range(-spawnRangeX,spawnRangeX);
            Vector3 position = new Vector3(xPos, 0, spawnPosZ);

            //Elegimos un indice aleatorio para el array
            int animalIndex = Random.Range(0,animalPrefabs.Length);

            //elegimos un animal aleatoria de la lista
            GameObject randomAnimal = animalPrefabs[animalIndex];

            Instantiate(randomAnimal, position,
                randomAnimal.transform.rotation);
        }
    }
}
