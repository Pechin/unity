﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 10f;
    private float horizontalInput;
    public float xRange = 15;
    private float verticalInput;
    public float zRange = 1f;
    public GameObject proyectil;
    private float topBound = 30;
    private float lowBound = -10;
    Vector3 currentOffset;

    // Start is called before the first frame update
    void Start()
    {
        currentOffset = transform.position;
    }

    // Update is called once per frame
    void Update()
    { //movimiento horizontal
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        transform.Translate(Vector3.right * speed * Time.deltaTime * horizontalInput);
        transform.Translate(Vector3.forward * speed * Time.deltaTime * verticalInput);

        //Desaparecer comida
        if (transform.position.z > topBound) {
            Destroy(proyectil);
        };

        //Desaparecer animalico

        if (transform.position.z > topBound) {
            Destroy(gameObject);
        } else if (transform.position.z< lowBound) {
           Destroy (gameObject); }
    

        //disparar comida
        if (Input.GetKeyDown(KeyCode.Space)) {
            Instantiate(proyectil, transform.position, proyectil.transform.rotation);
        };

        //Entra en el if cuando me salgo por la izquierda
        if (transform.position.x < -xRange)
        {
            transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
        }
        //Entra en el if cuando me salgo por la derecha
        if (transform.position.x > xRange)
        {
            transform.position = new Vector3(xRange, transform.position.y, transform.position.z);

        }



        //Entra en el if cuando me salgo por atras

        if (transform.position.z < -zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -zRange);
        }
        //Entra en el if cuando me salgo por delante
        if (transform.position.z > zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, zRange);

        }

    }
}
